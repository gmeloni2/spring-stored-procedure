# SpringBoot and Stored Procedure on Mysql #

In this Project we use the **spring-boot-starter-data-jdbc** module to invoke a Stored Procedure on Mysql server

### Prerequisites ###

* Java JDK 8 or greater installed on local machine
* Access to Mysql instance (I have used one instance in my local machine). If you want to install Myqsl on Ubuntu see [this](https://phoenixnap.com/kb/how-to-install-mysql-on-ubuntu-18-04) helpful tutorial

### How do I get set up? ###

#### Set up database, table, and stored procedure ####

Database creation:

    create database test_db;
    
Table creation:
    
    CREATE TABLE test_db.BOOKS(
			ID_REF INT NOT NULL AUTO_INCREMENT ,
			NAME VARCHAR(100) NOT NULL,
			PRICE DECIMAL(15, 2) NOT NULL,
			PUBLICATION_TS TIMESTAMP NOT NULL,
			PRIMARY KEY (ID_REF)
	);
		
Procedure creation:

	DELIMITER $$
	
	CREATE PROCEDURE get_price_by_id_and_name (
		IN  book_id int,
		IN  book_name varchar(100), 
		OUT book_price DECIMAL(15, 2),
		OUT book_publication_ts TIMESTAMP
	)
	BEGIN
		SELECT price, publication_ts
		INTO book_price, book_publication_ts 
		FROM BOOKS
		WHERE ID_REF = book_id AND name = book_name;
	END$$
	
	DELIMITER ;				  
    
    
    
Verify Table and stored procedure invocation

	--insert sample record
	insert into test_db.BOOKS(name, price, publication_ts) values ('first book', 23.33, CURRENT_TIMESTAMP());
	insert into test_db.BOOKS(name, price, publication_ts)  values ('second book', 46.66, CURRENT_TIMESTAMP());
	insert into test_db.BOOKS(name, price, publication_ts)  values ('third book', 99.99, CURRENT_TIMESTAMP());
    
    select * from BOOKS;
    
	Expected records:
    
	+--------+-------------+-------+---------------------+
	| ID_REF | NAME        | PRICE | PUBLICATION_TS      |
	+--------+-------------+-------+---------------------+
	|      1 | first book  | 23.33 | 2020-10-09 11:07:15 |
	|      2 | second book | 46.66 | 2020-10-09 11:07:37 |
	|      3 | third book  | 99.99 | 2020-10-09 11:07:38 |
	+--------+-------------+-------+---------------------+

#### Set up project ####

Edit **application.properties** under *src/main/resources*

	## MySQL
	spring.datasource.url=jdbc:mysql://host:port/test_db?serverTimezone=UTC&useLegacyDatetimeCode=false
	spring.datasource.username=****
	spring.datasource.password=****

#### Run project ####

    mvn spring-boot:run (on linux systems)
    .\mvnw.cmd spring-boot:run (on windows systems) 			

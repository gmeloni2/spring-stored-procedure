package com.example.demo;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;

@SpringBootApplication
public class TestStoredProcedureApplication implements CommandLineRunner {

  private static final Logger log = LoggerFactory.getLogger(TestStoredProcedureApplication.class);

  @Autowired
  @Qualifier("JDBCBookRepository")
  private BookRepository bookrepository;

  @Autowired
  StoredProcedureComponent storedProcedureComponent;

  public static void main(String[] args) {
    SpringApplication.run(TestStoredProcedureApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {

    log.info("populating BOOK table with sample record ...");

    List<Book> bookList = Arrays.asList(new Book("first book", BigDecimal.valueOf(23.33)),
        new Book("second book", BigDecimal.valueOf(46.66)),
        new Book("third book", BigDecimal.valueOf(99.99)));

    for (Book b : bookList) {
      bookrepository.save(b);
    }

    log.info("Record Inserted: " + bookrepository.count());
    List<Book> allBooks = bookrepository.getAllBooks();

    if (allBooks != null) {
      for (Book b : allBooks) {
        log.info(b.toString());
      }
    }

    // CALL get_price_by_id_and_name(xxx,'first book',@price);
    Book testBook = allBooks.get(0);
    log.info("Invoking CALL get_price_by_id_and_name({},{},@price)", testBook.getId(),
        testBook.getName());
    Book result = storedProcedureComponent.execute(testBook.getId(), testBook.getName());

    log.info("the price of the book '{}' with id '{}' is {}. (publication time: {})",
        testBook.getName(), testBook.getId(), result.getPrice(), result.getPublicationTs());

    log.info("deleting all records from BOOK table...");
    bookrepository.deleteRecords();
    log.info("Num Record in BOOK Table: " + bookrepository.count());

  }

}

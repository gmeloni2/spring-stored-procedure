package com.example.demo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import com.example.demo.model.Book;

@Component
public class StoredProcedureComponent {

  private static final Logger log = LoggerFactory.getLogger(StoredProcedureComponent.class);

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private SimpleJdbcCall storedProcedureCall;

  @PostConstruct
  public void init() {

    jdbcTemplate.setResultsMapCaseInsensitive(true);
    storedProcedureCall =
        new SimpleJdbcCall(jdbcTemplate).withProcedureName("get_price_by_id_and_name");

  }

  public Book execute(Long id, String name) {


    SqlParameterSource in =
        new MapSqlParameterSource().addValue("book_id", id).addValue("book_name", name);

    Map<String, Object> spResult = storedProcedureCall.execute(in);

    log.debug("The price for Book '{}' is {}", name, spResult.get("book_price"));

    Book book = new Book();
    book.setPrice((BigDecimal) spResult.get("book_price"));
    book.setPublicationTs((Timestamp) spResult.get("book_publication_ts"));
    return book;

  }

}

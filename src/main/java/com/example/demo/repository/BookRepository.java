package com.example.demo.repository;

import java.util.List;
import com.example.demo.model.Book;

public interface BookRepository {

  public int count();

  public boolean save(Book book);

  public List<Book> getAllBooks();

  public void deleteRecords();


}

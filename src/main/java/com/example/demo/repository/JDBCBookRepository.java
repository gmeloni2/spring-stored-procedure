package com.example.demo.repository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.example.demo.model.Book;


@Repository
public class JDBCBookRepository implements BookRepository {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Override
  public int count() {

    return jdbcTemplate.queryForObject("select count(*) from test_db.BOOKS", Integer.class);
  }

  @Override
  public boolean save(Book book) {
    int rowsAffected = jdbcTemplate.update(
        "insert into test_db.BOOKS(name, price, publication_ts) values (?, ?, ?)", book.getName(),
        book.getPrice(), book.getPublicationTs());
    return rowsAffected == 1 ? true : false;
  }

  @Override
  public void deleteRecords() {
    jdbcTemplate.update("delete from BOOKS");
  }

  @Override
  public List<Book> getAllBooks() {
    return jdbcTemplate.query("select * from test_db.BOOKS",
        (resultSet, rowNumber) -> new Book(resultSet.getLong("id_ref"), resultSet.getString("name"),
            resultSet.getBigDecimal("price"), resultSet.getTimestamp("publication_ts")));
  }

}

package com.example.demo.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Book {


  private Long id;
  private String name;
  private BigDecimal price;
  private Timestamp publicationTs;

  public Book() {}

  public Book(Long id, String name, BigDecimal price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public Book(Long id, String name, BigDecimal price, Timestamp publicationTs) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.publicationTs = publicationTs;
  }

  public Book(String name, BigDecimal price) {
    this.name = name;
    this.price = price;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Timestamp getPublicationTs() {
    return publicationTs;
  }

  public void setPublicationTs(Timestamp publicationTs) {
    this.publicationTs = publicationTs;
  }

  @Override
  public String toString() {
    return "Book [id=" + id + ", name=" + name + ", price=" + price + ", publicationTs="
        + publicationTs + "]";
  }
}

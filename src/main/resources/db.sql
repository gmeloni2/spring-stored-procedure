--table creation

CREATE TABLE test_db.BOOKS(
    ID_REF INT NOT NULL AUTO_INCREMENT ,
    NAME VARCHAR(100) NOT NULL,
    PRICE DECIMAL(15, 2) NOT NULL,
    PUBLICATION_TS TIMESTAMP NOT NULL,
    PRIMARY KEY (ID_REF)
);

--insert 

insert into test_db.BOOKS(name, price, publication_ts) values ('first book', 23.33, CURRENT_TIMESTAMP());
insert into test_db.BOOKS(name, price, publication_ts)  values ('second book', 46.66, CURRENT_TIMESTAMP());
insert into test_db.BOOKS(name, price, publication_ts)  values ('third book', 99.99, CURRENT_TIMESTAMP());

--check
select * from BOOKS;
+--------+-------------+-------+---------------------+
| ID_REF | NAME        | PRICE | PUBLICATION_TS      |
+--------+-------------+-------+---------------------+
|      1 | first book  | 23.33 | 2020-10-09 11:07:15 |
|      2 | second book | 46.66 | 2020-10-09 11:07:37 |
|      3 | third book  | 99.99 | 2020-10-09 11:07:38 |
+--------+-------------+-------+---------------------+


-- stored procedure

DELIMITER $$

CREATE PROCEDURE get_price_by_id_and_name (
	IN  book_id int,
	IN  book_name varchar(100), 
	OUT book_price DECIMAL(15, 2),
	OUT book_publication_ts TIMESTAMP
)
BEGIN
	SELECT price, publication_ts
	INTO book_price, book_publication_ts 
	FROM BOOKS
	WHERE ID_REF = book_id AND name = book_name;
END$$

DELIMITER ;

--check stored procedure

CALL get_price_by_id_and_name(1,'first book',@price, @ts);
SELECT @price, @ts;

+--------+---------------------+
| @price | @ts                 |
+--------+---------------------+
|  23.33 | 2020-10-09 11:07:15 |
+--------+---------------------+
